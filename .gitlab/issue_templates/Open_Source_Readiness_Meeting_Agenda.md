# Open Source Readiness Meeting Agenda

## Date and Time
**YYYYMMDD - time**

## Untracked Attendees
- Fullname, Affiliation, 
- ...

## Meeting Notices
- **Project Leads**: 
- **All Participants**: 

- **Antitrust Compliance**: 

- **Meeting Recordings**: 

## Agenda
- [ ] Convene & roll call (5mins)

- [ ] Review Meeting Notices (see above)
- [ ] Approve past meeting minutes
- [ ] Agenda item 1
- [ ] Agenda item 2
- [ ] ...
- [ ] AOB, Q&A & Adjourn (5mins)

## Decisions Made
- [ ] Decision 1
- [ ] Decision 2
- [ ] ...

## Action Items
- [ ] Action 1
- [ ] Action 2
- [ ] ...

