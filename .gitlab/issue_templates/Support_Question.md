# 🤗 Support Question

## Question Description
Describe your question in detail here. Please include specific details to help us understand your issue.

- **Context**: Explain the context or background in which this question arose.
- **Specific Problem**: If there is a specific problem or error you are encountering, describe it clearly.

## What You Have Tried
- **Research**: Mention if you have searched existing issues, documentation, or external sources for answers to your question.
- **Attempts to Resolve**: Describe any steps you have already taken to try and solve your issue.

## Additional Information
- **Screenshots**: If applicable, add screenshots to help explain your problem.
- **Environment**: Mention any relevant details about your environment (e.g., operating system, software version) that might help in addressing your question.

